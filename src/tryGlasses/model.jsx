import React, { Component } from "react";
import "./modelGlasses.css";
class Model extends Component {
  renderDetail = () => {
    if (this.props.selectedGlasses) {
      return (
        <div className="glasses__detail position-absolute">
          <h5>{this.props.selectedGlasses.name}</h5>
          <p>{this.props.selectedGlasses.desc}</p>
        </div>
      );
    }
  };
  render() {
    return (
      <div className="position-relative mx-auto" style={{ width: 300 }}>
        <img className="d-block w-100" src="./glassesImage/model.jpg" alt="" />
        {this.props.selectedGlasses && (
          <img
            className="position-absolute glassesModel"
            src={this.props.selectedGlasses.url}
            alt=""
          />
        )}
        {this.renderDetail()}
      </div>
    );
  }
}

export default Model;
