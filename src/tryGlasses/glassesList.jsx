import React, { Component } from 'react';
import GlassesItem from './glassesItem';

class GlassesList extends Component {
    displayGlasses = () => {
        return this.props.glassesList.map((item) => {
          return (
            <div key={item.id} className=" p-3">
              <GlassesItem
                glasses={item}
                handleTryGlasses={this.props.handleTryGlasses}
              />
            </div>
          );
        });
      };
    render() {
        return (
            <div className="d-flex flex-wrap justify-content-center">
               {this.displayGlasses()} 
            </div>
        );
    }
}

export default GlassesList;