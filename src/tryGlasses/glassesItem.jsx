import React, { Component } from "react";
import "./glassesItem.css"
class GlassesItem extends Component {
  render() {
    const { id, name, desc, url } = this.props.glasses;
    return (
      <div className="glassesItem">
        <div type="button"
          onClick={() => this.props.handleTryGlasses(this.props.glasses)}
          className="container text-center"
        >
          <img style={{ height: 50 }} src={url} alt="glasses" />
        </div>
      </div>
    );
  }
}

export default GlassesItem;
