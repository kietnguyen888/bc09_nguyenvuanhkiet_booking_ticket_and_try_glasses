import React, { Component } from "react";
import "./DanhSachGheDat.css"
class DanhSachGheDat extends Component {
  displayBookingSeat = () => {
    return this.props.bookingSeatList.map((item) => {
      return (
        <p >
          Ghế: {item.TenGhe}  ${item.Gia}
          <button onClick={()=>this.props.removeFromList(item)} className="mx-1">[Hủy]</button>
        </p>
      );
    });
  };
  render() {
    return <div>{this.displayBookingSeat()}
    </div>;
  }
}

export default DanhSachGheDat;
