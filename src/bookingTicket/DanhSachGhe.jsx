import React, { Component } from "react";
import GheItem from "./GheItem";

class DanhSachGhe extends Component {
  displaySeat = () => {
    return this.props.seatList.map((item) => {
      return (
        <div key={item.SoGhe} className="col-3 p-2">
          <GheItem
            seat={item}
            addToList={this.props.addToList}
            removeFromList={this.props.removeFromList}
            bookingSeatList={this.props.bookingSeatList}
          />
        </div>
      );
    });
  };
  render() {
    return (
      <div>
        <div className="container row">{this.displaySeat()}</div>
      </div>
    );
  }
}

export default DanhSachGhe;
