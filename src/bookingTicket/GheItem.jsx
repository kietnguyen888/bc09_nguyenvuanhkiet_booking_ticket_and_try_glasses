import React, { Component } from "react";
class GheItem extends Component {
  checkBookingList = () => {
    return this.props.bookingSeatList.findIndex((item) => {
      return item.SoGhe === this.props.seat.SoGhe;
    });
  };
  handleDisplaySeat = () => {
    const { SoGhe, TrangThai } = this.props.seat;

    if (TrangThai === true) {
      return (
        <button className="btn btn-danger" disabled>
          {SoGhe}
        </button>
      );
    } else if (this.checkBookingList() === -1) {
      return (
        <button
          onClick={() => this.props.addToList(this.props.seat)}
          className="btn btn-secondary text-white"
        >
          {SoGhe}
        </button>
      );
    } else {
      return (
        <button
          onClick={() => this.props.removeFromList(this.props.seat)}
          className="btn btn-success text-white"
        >
          {SoGhe}
        </button>
      );
    }
  };
  render() {
    return <div>{this.handleDisplaySeat()}</div>;
  }
}
export default GheItem;
